console.log("Connected");

class Product{
	constructor(name,price){
		this.name = name;
		this.price = price;
		this.isActive = true;
	}
	archive(){
		this.isActive = false;
		return this;
	}

	updatePrice(updatedPrice){
		this.price = updatedPrice;
		return this;
	}
}

let prodA = new Product("Apple",20);
let prodB = new Product("Orange",40);
console.log(prodA);
console.log(prodB);

class Cart{
	constructor(){
		this.contents = [];
		this.totalAmount = 0;
	}
	addToCart(product,quantity){
		this.contents.push({product,quantity});
		return this;
	}

	showCartContents(){
		console.log(this.contents);
		return this;
	}

	clearCartContents(){
		this.contents = [];
		return this;
	}

	computeTotal(){
		let sum = 0;
		this.contents.forEach(product => {
			sum += product.product.price*product.quantity;
		})
		this.totalAmount = sum;
		return this;
	}
}

// Ex.instantiation
let cart = new Cart();
let cart1 = new Cart();

cart.addToCart(prodA,2);
cart1.addToCart(prodB,4);

class Customer{
	constructor(email){
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}
	//Nalilito ako kung lahat ba ng cart or isa lng. Parang sa sample out isa lng, ginaya ko nalang hahaha.
	// Dih ko muna tapusin, need kopa po lutuan ulam mga kapatid ko hahaha.
	
	// checkOut(){
	// 	cart.computeTotal();
	// 	this.cart = cart;
	// 	let content = this.cart.contents;
	// 	let amount = this.cart.totalAmount;
	// 	if(this.cart !== [] && this.cart !== undefined){
	// 		this.orders.push({content,amount})
	// 	}
	// 	return this;
	// }
}

let john = new Customer("john@gmail.com");
console.log(john);
